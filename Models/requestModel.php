<?php
/**
 * Created by PhpStorm.
 * User: Tek
 * Date: 19.10.2014
 * Time: 12:50
 */

class requestModel {
    private $table = "requests";


    private $fieldsEtalon = array(
        "LicenseNum" => "02651",
        "LicenseDate" => "08.08.2011 0:00:00",
        "Name" => 'ООО "НЖТ-ВОСТОК"',
        "OgrnNum" => "1107746402246",
        "OgrnDate" => "17.05.2010 0:00:00",
        "Brand" => "FORD",
        "Model" => "FOCUS",
        "RegNum" => "ЕМ33377",
        "Year" => "2011",
        "BlankNum" => "002695",
    );
    public $fields = array(
        "id" => "",
        "LicenseNum" => "",
        "LicenseDate" => "",
        "Name" => "",
        "OgrnNum" => "",
        "OgrnDate" => "",
        "Brand" => "",
        "Model" => "",
        "RegNum" => "",
        "Year" => "",
        "BlankNum" => "",
        "Info" => "",
        "requestStatus" => "",
        "requestTime" => "",
        "headers" => "",
        "ping" => "",
    );
    function requestModel($propArr){
        $fieldKeys = array_keys($this->fields);
        foreach($propArr as $key => $value){
            if(in_array($key,$fieldKeys)){
                $this->fields["$key"] = $value;
            }
        }

    }

    function save(){
        $this->compareWithEtalon();
        if($this->fields["id"] != ""){
            $this->update();
        }else{
            $this->insert();
        }
    }

    function insert(){
        $this->toDate();
        Config::$dbObject->insert($this->table, $this->fields);
    }

    function update(){
        $this->toDate();
        Config::$dbObject->update($this->table, $this->fields, "id = ".$this->fields["id"]);
    }

    function compareWithEtalon(){
        $equal = true;
        foreach($this->fieldsEtalon as $key => $val){
            if(!array_key_exists($key,$this->fields) || $this->fields[$key] !== $val){
                $equal = false;
                break;
            }
        }
        $this->fields["requestStatus"] = ($equal)?"OK":"FALSE";
    }

    //todo доделать
    function toDate(){
        if($this->fields["LicenseDate"] != "") {
            $this->fields["OgrnDate"] = DateTime::createFromFormat("d.m.Y h:i:s", $this->fields["OgrnDate"])->getTimestamp();
        }

        if($this->fields["LicenseDate"] != "") {
            $this->fields["LicenseDate"] = DateTime::createFromFormat("d.m.Y h:i:s", $this->fields["LicenseDate"])->getTimestamp();
        }
    }
}
