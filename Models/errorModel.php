<?php

class errorModel {

	const DEFAULT_ERROR_CODE = 'INTERNAL_ERROR';
	const DEFAULT_PARAM = null;

	static private $errors;

	private $code = errorModel::DEFAULT_ERROR_CODE;
	private $desc = '';
	private $params = array();

	function __construct($code = errorModel::DEFAULT_ERROR_CODE, $param = errorModel::DEFAULT_PARAM, $desc = null) {
		$this->code = ($code)? $code : errorModel::DEFAULT_ERROR_CODE;
		$this->desc = ($desc)? $desc : static::getErrorsList()[$code];
		if($param) $this->params[] = $param;
	}

	private function getErrorsList() {
		if(!empty(static::$errors)) return static::$errors;
		static::$errors = include Config::$rootPath . '/Config/errors.php';
		return static::$errors;
	}

	public function as_array() {
		return array('code' => $this->code, 'description' => $this->desc, 'parameters' => $this->params);
	}

}