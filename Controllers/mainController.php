<?php

/**
 * Created by PhpStorm.
 * User: Tek
 * Date: 17.10.2014
 * Time: 20:13
 */
class mainController
{
    function actionIndex()
    {
        try {
            $timeBeforeReq = helpersService::getCurrentMicrosecondsTime();
            $client = new SoapClient("http://82.138.16.126:8888/TaxiPublic/Service.svc?wsdl", array('trace' => 1, "connection_timeout" => 15));
            $par = $client->GetTaxiInfos(array('request' => array('RegNum' => 'ем33377')));
            $timeAfterReq = helpersService::getCurrentMicrosecondsTime();

            $parArr = (array)$par->GetTaxiInfosResult->TaxiInfo;
            $parArr["ping"] = $timeAfterReq - $timeBeforeReq;
            $parArr["requestTime"] = time();
            $parArr["headers"] = $client->__getLastResponseHeaders();

            $request = new requestModel($parArr);
            $request->save();
        } catch (Exception $e) {
            ErrorCollectorService::getInstance()->add(new errorModel('INTERNAL_ERROR', null));
        }
    }

    function actionGetAll()
    {
        $selected = Config::$dbObject->select("requests");
        if (is_array($selected)) {
            $requestCollection = [];
            foreach ($selected as $one) {
                $requestCollection[] = new requestModel($one);
            }
            DataCollectorService::getInstance()->add($requestCollection);
        }
    }
} 