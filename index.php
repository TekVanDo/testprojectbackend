<?php
header('Access-Control-Allow-Origin: *');
mb_internal_encoding("UTF-8");
include("Config/Config.php");
Config::$rootPath = $_SERVER["DOCUMENT_ROOT"];
include(Config::$rootPath."/Services/autoloadService.php");
Config::$dbObject = new dbService(Config::$dbConnection,Config::$dbUser,Config::$dbPassword);

function dbException(){
    ErrorCollectorService::getInstance()->add(new errorModel('DB_ERROR', null));
}

Config::$dbObject->setErrorCallbackFunction("dbException","text");

routeService::route();
routeService::responseJson();