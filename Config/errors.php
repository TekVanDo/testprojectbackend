<?php
return array(
	'NOT_FOUND' => 'Resource not found',
	'FS_ERROR' => 'File System request failed',
	'DB_ERROR' => 'Data base request failed',
	'CANNOT_BE_NULL' => 'Resource cannot be empty',
	'WRONG_VALUE' => 'Value is invalid',
	'VALIDATION_ERROR' => 'Resource could not pass validation',
	'INTERNAL_ERROR' => 'Some problems inside service. Contact with developers.',
	'PERMISSION_DENIED' => 'Not enough rights to perform this operation',
	'DATA_MISSING' => 'Required data is missing',
	'AUTHORISATION_FAILED' => 'Wrong email or password',
	'INTEGRITY_VIOLATION' => 'You trying to save contradictory data. For instance, group_id and institution_id simultaneously, but group has another institution.',
	'BAD_REQUEST' => 'Request is not valid',
	'KEY_OUTDATED' => 'Hash key are outdated'
);
