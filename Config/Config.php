<?php
class Config
{
    static $rootPath = "";
    static $dbConnection = "mysql:host=localhost;port=3306;dbname=test";
    static $dbObject = false;
    static $dbUser = "root";
    static $dbPassword = "";
    static $defaultController = "main";
    static $defaultAction = "Index";
}