<?php

abstract class BaseCollectorService {

	protected $data = array();

	protected static $_instance;

	public function clear() {
		$this->data = array();
	}

	public function has() {
		return count($this->data) > 0;
	}

	public function get() {
		return $this->data;
	}

	public function add($data) {
		if(is_array($data)) {
			foreach($data as $row) {
				$this->add($row);
			}
		} elseif ($data) {
			$this->data[] = $this->prepare($data);
		}
	}

	protected function prepare($data) {
		if(is_object($data) && method_exists($data, 'as_array')) return $data->as_array();
		return $data;
	}

	public static function getInstance() {
		if (null === static::$_instance) {
			static::$_instance = new static();
		}
		return static::$_instance;
	}

}