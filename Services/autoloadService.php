<?php
function __autoload($class_name) {
    $split = preg_split("/(?<=[a-z])(?![a-z])/", $class_name, -1, PREG_SPLIT_NO_EMPTY);
    switch($split[count($split)-1]){
        case 'Controller':
            $folder = 'Controllers';
            break;
        case 'Model':
            $folder = 'Models';
            break;
        case 'Service':
            $folder = 'Services';
            break;
        default:
            $folder = false;
            break;
    }
    if($folder) {
        include Config::$rootPath.'/'. $folder .'/' . $class_name . '.php';
    }else{
        ErrorCollectorService::getInstance()->add(new errorModel('INTERNAL_ERROR', null));
        routeService::responseJson();
        die();
    }
}