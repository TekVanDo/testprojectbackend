<?php
/**
 * Created by PhpStorm.
 * User: Tek
 * Date: 17.10.2014
 * Time: 21:48
 */

class routeService {
    static function route(){
        if(isset($_REQUEST["page"]) && !empty($_REQUEST["page"])){
            $controllerString = trim($_REQUEST["page"]);
        }else{
            $controllerString = Config::$defaultController;
        }
        $controllerString = trim($controllerString)."Controller";

        if(isset($_REQUEST["action"]) && !empty($_REQUEST["action"])){
            $actionString =trim($_REQUEST["action"]);
        }else{
            $actionString = Config::$defaultAction;
        }
        $actionString = "action".trim($actionString);

        $controller = new $controllerString();
        $controller->$actionString();
    }

    static function responseJson(){
        $responseArr = [
            "data" => DataCollectorService::getInstance()->get(),
            "error" => ErrorCollectorService::getInstance()->get(),
        ];
        echo (json_encode($responseArr));
    }
} 